import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vant from 'vant' 
import 'vant/lib/index.css'; // 全局引入样式
import './utils/rem'
import 'ant-design-vue/dist/antd.css';
import Antd from 'ant-design-vue';

createApp(App).use(router).use(store).use(vant).use(Antd).mount('#app')
